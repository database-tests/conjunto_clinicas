#!/bin/bash

USER=$(whoami)
DATABASE="conjunto_clinicas"
dim_tempo_data=(
"('31', '11', '2', '1', '2021')"
"('32', '12', '3', '4', '2021')"
"('33', '10', '3', '6', '2021')"
)

dim_medico_data=(
"('21', 'Jefferson', 'Cirurgia')"
"('22', 'Bruno', 'Dentista')"
"('23', 'Isadora', 'Massagista')"
)

dim_paciente_data=(
"('11', 'M', '15-20')"  
"('12', 'F', '20-25')"  
"('13', 'F', '12-30')"  
)

dim_clinica_data=(
"('01', 'Santa Luzia')" 
"('02', 'Selso Ramos')" 
"('03', 'Infantil')"  
)

fact_consulta_data=(
"('31', '22', '12', '02', '3', '1000')" 
"('32', '23', '11', '03', '1', '500')" 
)

for dim_tempo in "${dim_tempo_data[@]}"
do
psql -d $DATABASE -c "INSERT INTO DIM_TEMPO (ID_TEMPO, MES, TRIMESTRE, SEMESTRE, ANO) VALUES ${dim_tempo};"
done

for dim_medico in "${dim_medico_data[@]}"
do
psql -d $DATABASE -c "INSERT INTO DIM_MEDICO (ID_MEDICO, NOME_MEDICO, ESPECIALIDADE_MEDICA) VALUES ${dim_medico};"
done

for dim_paciente in "${dim_paciente_data[@]}"
do
psql -d $DATABASE -c "INSERT INTO DIM_PACIENTE (ID_PACIENTE, SEXO, FAIXA_ETARIA) VALUES ${dim_paciente};"
done

for dim_clinica in "${dim_clinica_data[@]}"
do
psql -d $DATABASE -c "INSERT INTO DIM_CLINICA (ID_CLINICA, NOME_CLINICA) VALUES ${dim_clinica};"
done

for fact_consulta in "${fact_consulta_data[@]}"
do
psql -d $DATABASE -c "INSERT INTO FACT_CONSULTA (ID_TEMPO, ID_MEDICO, ID_PACIENTE, ID_CLINICA, NRO_CONSULTAS, VLR_CONSULTAS) VALUES ${fact_consulta};"
done