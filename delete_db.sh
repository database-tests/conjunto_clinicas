#!/bin/bash

USER=$(whoami)

DATABASE="conjunto_clinicas"

if psql -lqt | cut -d \| -f 1 | grep -qw $DATABASE; then
    echo "Removendo o banco de dados ${DATABASE}..."
    dropdb -U "${USER}" --force $DATABASE
    echo "Banco de dados removido com sucesso!"
else
    echo "O banco de dados tv_store não existe."
fi
