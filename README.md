# CONJUNTO_CLINICAS

Essas scripts foram escritas com intuito de automatizar uma atividade de faculdade.

## Instalar o PostgreSQL

 * Para instalar o PostgreSQL siga o passo a passo do site: https://www.postgresql.org/download/
 * Procedimento de instalação no Ubuntu:
```
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
```
```
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
```
```
sudo apt-get update
```
```
sudo apt-get -y install postgresql
```

## Configurar usuário
 * Configure seu usuario para trabalhar no PostgreSQL:
```
sudo -u postgres -i
```
```
createuser -l -d -P <SEU_USUARIO>
```

## Modo de usar
 * Cria o banco de dados e sua estrutura
```
./create_db.sh
```
  * Insere os dados nas tabelas
```
./insert_db.sh
```
 * Exibe todas as tabelas
```
./show_db.sh
```
  * Limpa os dados de todas as tabelas
```
./remove_values_db.sh
```
 * Deleta o banco de dados
```
./delete_db.sh
```

## Diagrama
